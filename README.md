docker-compose-medialib-databaseserver
====================

dockercompose file for basic mariadb database server

Contains:
- mariadb database container


Contents
-------------
- dockercompose.yml
- env.template
- mysqlconfig/medialib.cnf

General
-------------
Mariadb database server
Be warned that port 3306 is exposed, use firewall rules to prevent world access. 
custom mysqlsettings in medialib.cnf

Run container
-------------
- copy env.template to .env and adjust variables


```
docker-compose up -d
```





